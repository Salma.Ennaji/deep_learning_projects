#%% Definition of the class
#basic imports
CE, CU, CR, CG, CY, CB, CV, CT, CW, CK, CGR = '\33[0m','\33[4m','\33[31m','\33[32m','\33[33m','\33[34m','\33[35m','\33[36m','\33[37m', '\33[30m', '\33[90m'
import torch
import numpy as np
from typing import List, Tuple
import math
torch.set_grad_enabled(False) #as asked

def index2onehot(array_target: torch.Tensor) -> torch.Tensor:
    """
    convert a index of classes to one-hot encoding
    """
    n_samples = len(array_target)
    n_classes = 2
    tt = torch.zeros((n_samples,n_classes))
    for i,s in enumerate(array_target):
        tt[i,s] = 1
    return tt

def generate_disc_set(nb, one_hot=False, seed=None, radius: int=1/torch.sqrt(torch.tensor(torch.pi*2))):
    """
    generates a dataset of nb samples of 2D points with labels 0 or 1
    """
    if(seed is not None):
        torch.manual_seed(seed)
    center = torch.tensor([0.5,0.5])
    radius = radius #1/torch.sqrt(torch.tensor(torch.pi*2))
    xy = torch.rand(nb,2)
    target = ((xy-center).pow(2).sum(axis=1) > radius**2).to(torch.int64)
    if(one_hot):
        target = index2onehot(target)
    return xy, target


class Tanh():
    def __init__(self, name: str="Tanh"):
        self.__name__ = name
        self.s: torch.Tensor = None
    def __call__(self, x):
        self.s = self.tanh(x)
        self.s[torch.where(x>10)] = 1 #to avoid inf/inf=nan
        self.s[torch.where(x<-10)] = -1
        return self.s
    def tanh(self, x):
        return torch.div((torch.exp(x) - torch.exp(-x)),(torch.exp(x) + torch.exp(-x)))
    def backward(self, dl_ds):
        return (1 - self.tanh(self.s)**2) * dl_ds #derivative of tanh(x) = 1-tanh(x)**2
    def param(self):
        return []
    def zero_grad(self):
        pass
    def update(self, *args):
        pass


class Sigmoid():
    def __init__(self, name: str="Sigmoid"):
        self.__name__ = name
        self.s: torch.Tensor = None
    def sigmoid(self, x):
        return 1 / (1 + torch.exp(-x))
    def __call__(self, x):
        self.s = 1 / (1 + torch.exp(-x))
        return self.s
    def backward(self, dl_ds):
        return  self.sigmoid(self.s) * (1 - self.sigmoid(self.s)) * dl_ds #derivative of sigma(x) = sigma(x)*(1-sigma(x))
    def param(self):
        return []
    def zero_grad(self):
        pass
    def update(self, *args):
        pass

class ReLU():
    def __init__(self, name: str='ReLU') -> None:
        self.__name__ = name
        self.s: torch.Tensor = None
    def __call__(self, x):
        self.s = x.clamp(min=0)
        return self.s #clamp is the same as max(0,x)
    def backward(self, dl_ds):
        return (torch.tensor(self.s)>0).to(torch.float32)*dl_ds #returns 1 if x>0, 0 otherwise
    def param(self):
        return []
    def zero_grad(self):
        pass
    def update(self, *args):
        pass

class Linear():
    """
    linear layer
    """
    def __init__(self, n_in: int, n_out: int, name: str='Linear') -> None:
        self.__name__ = name
        std = 1 * math.sqrt(2.0/(n_in+n_out)) #xavier initialization
        self.w = torch.empty(n_out, n_in).normal_(std)
        self.b = torch.empty(n_out).normal_(std)
        self.dl_dw = torch.zeros(n_out, n_in)
        self.dl_db = torch.zeros(n_out)
        self.x: torch.Tensor = None

    def __call__(self, x: torch.Tensor):
        self.x = x
        return self.x.mm(self.w.t()) + self.b

    def backward(self, dl_ds: torch.Tensor):
        self.dl_dw.add_(dl_ds.t().mm(self.x))
        self.dl_db.add_(dl_ds.sum(axis=0))
        return dl_ds.mm(self.w)

    def param(self):
        return [self.w, self.b]
    def zero_grad(self):
        self.dl_dw.zero_()
        self.dl_db.zero_()
    def update(self, learning_rate):
        self.w -= learning_rate * self.dl_dw
        self.b -= learning_rate * self.dl_db


class Loss():
    """
    MSE loss
    """
    def __init__(self, name: str='Loss') -> None:
        self.__name__ = name
        pass
    def __call__(self, v: torch.Tensor, t: torch.Tensor) -> float:
        """ returns a float because a single dimension tensor is returned """
        return torch.sum(torch.pow(v-t,2))
    def backward(self, v: torch.Tensor, t: torch.Tensor) -> torch.Tensor:
        return 2 * (v - t)
    def param(self):
        return []
    def zero_grad(self):
        pass
    def update(self, *args):
        pass


class Module(object):
    def __init__(self, learning_rate=0.1, classification_threshold: float=0.5) -> None:
        self.operations_l = []
        self.loss = Loss() #default value
        self.learning_rate = learning_rate
        self.classification_threshold = classification_threshold
        self.error_rates = []
        self.error_rates_valid = []


    def set_classification_threshold(self, threshold: float=None):
        """
        autmatically set threshold setting for classification
        0.5 for sigmoid and 0 for tanh
        """
        if(threshold is None):
            if("sigmoid" in self.operations_l[-1].__name__.lower()): #if last operation is sigmoid
                self.classification_threshold = 0.5
            elif("tanh" in self.operations_l[-1].__name__.lower()): #if last operation is tanh
                self.classification_threshold = 0
            else:
                raise ValueError('unable to set threshold automatically, please set it manually.') #ERROR
        else:
            self.classification_threshold = threshold

    def Sequential(self, *modules):
        """
        sets new modules which applies the modules in sequence.
        """
        self.operations_l = modules

    def zero_grad(self):
        for op in self.operations_l:
            op.zero_grad()

    def param(self) -> List:
        """
        Returns a list of all the parameters of the module
        should return a list of pairs, each composed of a parameter tensor, and a gradient tensor of same size.
        This list should be empty for parameterless modules (e.g. ReLU).
        """
        param_l = []
        for cur_module in self.operations_l:
            param_l += cur_module.param()
        return param_l

    def forward_pass(self, input: torch.Tensor):
        """
        does a forward pass by applying each modules in sequence.
        """
        for cur_module in self.operations_l:
            input = cur_module(input)
        return input

    def backward_pass(self, dloss: torch.Tensor):
        """
        does a backward pass by applying each modules in reverse order.
        """
        dl_ds = dloss
        for cur_module in self.operations_l[::-1]:
            dl_ds = cur_module.backward(dl_ds)
        return dl_ds


    def predict(self, input: torch.Tensor):
        """ predict class 0 or 1 in a Tensor"""
        return [x.item() for x in (self.forward_pass(input)>self.classification_threshold).int()]

    def compute_error(self, input: torch.Tensor, target: torch.Tensor) -> float:
        """
        compute the error rate
        Note : for Tanh, train_target is in [-1,1] so we need to convert it to [0,1]
        """
        fp = self.predict(input)
        return np.mean([x!=y for x,y in zip(fp, target)])

    def train(self,
              train_input: torch.Tensor,
              train_target: torch.Tensor,
              nb_epochs: int,
              validation_input: torch.Tensor=None,
              validation_target: torch.Tensor=None) -> Tuple[List]:
        n_samples = train_input.size(0)
        print("training... with", nb_epochs, "epochs", 'learning rate:', self.learning_rate, 'n_samples:', train_input.shape[0])
        losses = []
        losses_valid = []
        self.error_rates = []
        self.error_rates_valid = []
        for e in range(nb_epochs): #for each epoch
            loss_epoch = 0
            loss_epoch_valid = 0
            self.zero_grad()
            for i in range(n_samples): #for each sample
                #___ Forward on training set
                temp = train_input.narrow(0, i, 1)
                temp = self.forward_pass(temp)
                cur_target = train_target.narrow(0,i,1)
                cur_loss = self.loss(temp.squeeze(), cur_target)
                loss_epoch += cur_loss

                #___ Forward on testing set
                if(validation_input is not None):
                    temp = self.forward_pass(validation_input.narrow(0,i,1))
                    cur_target = validation_target.narrow(0,i,1)
                    cur_loss = self.loss(temp.squeeze(), cur_target)
                    loss_epoch_valid += cur_loss
                #___ Backward
                dl_ds = self.loss.backward(temp, cur_target)
                dl_ds = self.backward_pass(dl_ds)
                #___ update parameters (weights and biases)
                for cur_operation in self.operations_l:
                    cur_operation.update(self.learning_rate)
                self.zero_grad() #after updating parameters, reset the gradients

            #___ Compute error rate
            error_rate = self.compute_error(train_input, train_target)
            self.error_rates.append(error_rate)
            if(validation_input is not None):
                error_rate_valid = self.compute_error(validation_input, validation_target)
                self.error_rates_valid.append(error_rate_valid)

            #___ printing: prints 10 times during training and at last iteration
            if(e%int(nb_epochs/10)==0 or e==nb_epochs-1):
                if(validation_input is not None):
                    #error_rate_valid = self.compute_error(validation_input, validation_target)
                    print(f'epoch {e+1}: loss = {loss_epoch}', 'error_rate=', error_rate, 'loss_valid=', loss_epoch_valid, 'error_rate_valid=', error_rate_valid)
                else:
                    print(f'epoch {e+1}: loss = {loss_epoch}', 'error_rate=', error_rate)
            losses_valid.append(loss_epoch_valid)
            losses.append(loss_epoch)

        if(validation_input is not None):
            return (losses, losses_valid)
        else:
            return (losses)



